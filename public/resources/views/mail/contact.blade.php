<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
	<meta charset="utf-8"> <!-- utf-8 works for most cases -->
	<meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn't be necessary -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
	<meta name="x-apple-disable-message-reformatting">  <!-- Disable auto-scale in iOS 10 Mail entirely -->
	<title>CPCELP - Contacto</title> <!-- The title tag shows in email notifications, like Android 4.4. -->

	<!-- Web Font / @font-face : BEGIN -->
	<!-- NOTE: If web fonts are not required, lines 10 - 27 can be safely removed. -->

	<!-- Desktop Outlook chokes on web font references and defaults to Times New Roman, so we force a safe fallback font. -->
	<!--[if mso]>
		<style>
			* {
				font-family: Arial, Helvetica, sans-serif !important;
			}
		</style>
	<![endif]-->

	<!-- All other clients get the webfont reference; some will render the font and others will silently fail to the fallbacks. More on that here: http://stylecampaign.com/blog/2015/02/webfont-support-in-email/ -->
	<!--[if !mso]><!-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
	<!--<![endif]-->

	<!-- Web Font / @font-face : END -->

	<!-- CSS Reset : BEGIN -->
	<style>
		html,
		body {
			margin: 0 auto !important;
			padding: 0 !important;
			height: 100% !important;
			width: 100% !important;
			font-family: Arial, Helvetica, sans-serif;
		}
		* {
			-ms-text-size-adjust: 100%;
			-webkit-text-size-adjust: 100%;
		}
		div[style*="margin: 16px 0"] {
			margin: 0 !important;
		}
		table,
		td {
			mso-table-lspace: 0pt !important;
			mso-table-rspace: 0pt !important;
		}
		table {
			border-spacing: 0 !important;
			border-collapse: collapse !important;
			table-layout: fixed !important;
			margin: 0 auto !important;
		}
		table table table {
			table-layout: auto;
		}
		img {
			-ms-interpolation-mode:bicubic;
		}
		a {
			text-decoration: none;
		}
		*[x-apple-data-detectors],  /* iOS */
		.unstyle-auto-detected-links *,
		.aBn {
			border-bottom: 0 !important;
			cursor: default !important;
			color: inherit !important;
			text-decoration: none !important;
			font-size: inherit !important;
			font-family: inherit !important;
			font-weight: inherit !important;
			line-height: inherit !important;
		}
		.im {
			color: inherit !important;
		}
		.a6S {
			display: none !important;
			opacity: 0.01 !important;
		}
		img.g-img + div {
			display: none !important;
		}
		@media only screen and (min-device-width: 320px) and (max-device-width: 374px) {
			u ~ div .email-container {
				min-width: 320px !important;
			}
		}
		@media only screen and (min-device-width: 375px) and (max-device-width: 413px) {
			u ~ div .email-container {
				min-width: 375px !important;
			}
		}
		@media only screen and (min-device-width: 414px) {
			u ~ div .email-container {
				min-width: 414px !important;
			}
		}

	</style>
	<!-- CSS Reset : END -->
	<!-- Reset list spacing because Outlook ignores much of our inline CSS. -->
	<!--[if mso]>
	<style type="text/css">
		ul,
		ol {
			margin: 0 !important;
		}
		li {
			margin-left: 30px !important;
		}
		li.list-item-first {
			margin-top: 0 !important;
		}
		li.list-item-last {
			margin-bottom: 10px !important;
		}
	</style>
	<![endif]-->
	<style>
		/* Media Queries */
		@media screen and (max-width: 480px) {
			.fluid {
				width: 100% !important;
				max-width: 100% !important;
				height: auto !important;
				margin-left: auto !important;
				margin-right: auto !important;
			}
			p {
				font-size: 14px !important;
			}
			p.prestamo {
				font-size: 24px !important;
			}
			.logo1 {
				width: 120px;
			}
			.logo2 {
				width: 120px;
			}
			p.tits {
				font-size: 18px !important;
			}
		}

	</style>
	<!--[if gte mso 9]>
	<xml>
		<o:OfficeDocumentSettings>
			<o:AllowPNG/>
			<o:PixelsPerInch>96</o:PixelsPerInch>
		</o:OfficeDocumentSettings>
	</xml>
	<![endif]-->
</head>
<body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #FFFFFF;">
	<center style="width: 100%; background-color: #FFFFFF;">
	<!--[if mso | IE]>
	<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color: #FFFFFF;">
	<tr>
	<td>
	<![endif]-->

		<!-- Visually Hidden Preheader Text : BEGIN -->
		<div style="display: none; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: sans-serif;">
			Gracias por contactarte!
		</div>
		<!-- Visually Hidden Preheader Text : END -->

		<!-- Preview Text Spacing Hack : BEGIN -->
		<div style="display: none; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: sans-serif;">
			&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;
		</div>
		<!-- Preview Text Spacing Hack : END -->

		<div style="max-width: 600px; margin: 0 auto;" class="email-container">
			<!--[if mso]>
			<table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="680">
			<tr>
			<td>
			<![endif]-->

			<!-- Email Body : BEGIN -->
			<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: 0 auto;">
				<!-- header : BEGIN -->
				<tr>
					<td style="background-color: #387F4A; background-repeat: no-repeat; background-position: botton right; padding-bottom: 20px">
						<table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: 0 auto;">
							<tr>
								<td style="text-align: center; padding: 50px 0 20px">
									<p style="color: #ffffff; font-family: 'Montserrat', sans-serif; font-size: px; margin: 0">Consejo Profesional<br> de Ciencias Económicas<br> de La Pampa</p>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<!-- header : END -->

				<!-- 1 Column Text + Button : BEGIN -->
				<tr>
					<td style="background-color: #ffffff;">
						<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">

							<tr>
								<td style="padding:10px 0; font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 20px; color: #555555; text-align:center">
									<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
										<tr>
											<td style="padding: 0">
												<p
													style="color: #3b3b3b; font-size: 18px; font-family: sans-serif; font-weight: 400; line-height: 28px">
													Muchas gracias <strong>{{ $name }}</strong> por contactarte con nosotros.<br>En breve nos comunicaremos con vos.</p>
											</td>
										</tr>

										<tr>
											<td style="padding: 30px 0">
												<p style="color: #3b3b3b; font-size: 18px; font-family: 'Montserrat', sans-serif; font-weight: 700;"
													class="tits">Contactanos</p>
											<p style="color: #387f4a; font-size: 24px; font-family: 'Montserrat', sans-serif; font-weight: 700;" class="tits">02954-773800</p>
											<p style="color: #387f4a; font-size: 24px; font-family: 'Montserrat', sans-serif; font-weight: 700;" class="tits">0810-243-0050</p>
											</td>
										</tr>

									</table>
								</td>
							</tr>

							<!-- footer -->
							<tr>
								<td style="padding:40px 40px; border-top: 1px solid #e7e7e7">
									<table cellspacing="0" cellpadding="0" border="0" width="100%">
										<tr>
											<td style="text-align: center">
												<!--<a href="#" style="padding: 20px">
													<img src="" alt="Twitter">
												</a>-->
												<!--<a href="#" style="padding: 20px">
													<img src="" alt="Instagram">
												</a>-->
												<a href="https://www.facebook.com/cpcelapampa/" style="padding: 20px">
													<img height="30" src="https://es-la.facebookbrand.com/wp-content/uploads/2019/04/FindUsOn_ESLA_Header_2019.png" alt="Facebook">
												</a>
												<!--<a href="#" style="padding: 20px">
													<img src="" alt="Whatsapp">
												</a>-->
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<!-- footer -->
						</table>
				</td>
			</tr>
		</table>
			<!-- Email Body : END -->

			<!--[if mso]>
			</td>
			</tr>
			</table>
			<![endif]-->
		</div>

	<!--[if mso | IE]>
	</td>
	</tr>
	</table>
	<![endif]-->
	</center>
</body>
</html>