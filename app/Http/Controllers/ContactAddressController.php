<?php

namespace App\Http\Controllers;

use App\Models\ContactAddress;
use Illuminate\Http\Request;

class ContactAddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ContactAddress::orderBy('subject', 'asc')->get();
    }

    public function webIndex()
    {
        return ContactAddress::select('id', 'subject')->orderBy('subject', 'asc')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'subject' => 'required|string',
            'email' => 'required|email'
        ]);

        if ($validator->fails()) {
            return [
                'success' => false,
                'message' => $validator->errors()->first()
            ];
        }

        $contactAddress = ContactAddress::create([
            'subject' => $request->subject,
            'email' => $request->email
        ]);

        return ['success' => true, 'id' => $contactAddress->id];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ContactAddress  $contactAddress
     * @return \Illuminate\Http\Response
     */
    public function show(ContactAddress $contactAddress)
    {
        return $contactAddress;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ContactAddress  $contactAddress
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContactAddress $contactAddress)
    {
        $validator = Validator::make($request->all(), [
            'subject' => 'required|string',
            'email' => 'required|email'
        ]);

        if ($validator->fails()) {
            return [
                'success' => false,
                'message' => $validator->errors()->first()
            ];
        }

        $contactAddress->subject = $request->subject;
        $contactAddress->email = $request->email;
        $contactAddress->save();

        return ['success' => true, 'id' => $contactAddress->id];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ContactAddress  $contactAddress
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContactAddress $contactAddress)
    {
        $contactAddress->delete();
    }
}
