<?php

namespace App\Http\Controllers;

use App\Models\Doctors;
use App\Models\Upload;
use Validator;
use Illuminate\Http\Request;

class DoctorsController extends Controller
{

    public function webIndex(Request $request)
    {
        $doctors = Doctors::query();
        $doctors = $doctors->paginate($request->size, ['*'], 'page', $request->page + 1);

        return $doctors;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $doctors = Doctors::query();

        if (!empty($request->searchText)) {
            $search = '%' . $request->searchText . '%';
            if (is_numeric($request->searchText)) {
                $doctors = $doctors->where('enrollment', 'LIKE', $search);
            } else {
                $doctors = $doctors->where('name', 'LIKE', $search)
                    ->orWhere('lastname', 'LIKE', $search)
                    ->orWhere('specialty', 'LIKE', $search);
            }
        }


        $doctors = $doctors->paginate($request->size, ['*'], 'page', $request->page + 1);

        return $doctors;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required|string',
                'lastname' => 'required|string',
                'specialty' => 'required|string',
                'enrollment' => 'required|numeric|min:1'
            ]
        );

        if ($validator->fails()) {
            return [
                'success' => false,
                'message' => $validator->errors()->first()
            ];
        }

        $doctor = Doctors::create(
            [
                'name' => $request->name,
                'lastname' => $request->lastname,
                'specialty' => $request->specialty,
                'enrollment' => $request->enrollment,
            ]
        );


        return ['success' => true, 'message' => 'Profesional creado con exito.'];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Doctors  $doctors
     * @return \Illuminate\Http\Response
     */
    public function show(Doctors $doctors)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Doctors  $doctors
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Doctors $doctors, $id)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required|string',
                'lastname' => 'required|string',
                'specialty' => 'required|string',
                'enrollment' => 'required|numeric|min:1',
                'image_id' => 'nullable|numeric|min:1'
            ]
        );

        if ($validator->fails()) {
            return [
                'success' => false,
                'message' => $validator->errors()->first()
            ];
        }

        $doctor = Doctors::findOrFail($id);
        $doctor->name = $request->name;
        $doctor->lastname = $request->lastname;
        $doctor->specialty = $request->specialty;
        $doctor->enrollment = $request->enrollment;
        $doctor->image_id = $request->image_id;

        $doctor->save();

        return ['success' => true, 'message' => 'Profesional actualizado con exito.'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Doctors  $doctors
     * @return \Illuminate\Http\Response
     */

    public function destroy(Doctors $doctor, $id, $uploadId)
    {
        Doctors::findOrFail($id)->delete();
        $uploadObject = Upload::findOrFail($uploadId);
        Upload::destroyFile($uploadObject);
        return ['success' => true];
    }

    private function getImage($doctor)
    {
        $upload = Upload::find($doctor->image_id);
        if ($upload) {
            return url('media/image/doctors/' . $upload->file);
        } else {
            return url('media/image/doctors/' . Upload::first()->file);
        }
    }
}
