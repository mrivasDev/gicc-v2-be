<?php

namespace App\Http\Controllers;

use App\Models\News;
use App\Models\Upload;
use Validator;
use Illuminate\Http\Request;

class NewsController extends Controller
{

    public function webIndex(Request $request)
    {
        $news = News::query();
        $news = $news->paginate($request->size, ['*'], 'page', $request->page + 1);

        return $news;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $news = News::query();

        if (!empty($request->searchText)) {
            $search = '%' . $request->searchText . '%';
            $news = $news->where('title', 'LIKE', $search);
        }


        $news = $news->paginate($request->size, ['*'], 'page', $request->page + 1);

        return $news;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'title' => 'required|string',
                'body' => 'required|string',
                'listed' => 'required|boolean'
            ]
        );

        if ($validator->fails()) {
            return [
                'success' => false,
                'message' => $validator->errors()->first()
            ];
        }

        $news = News::create(
            [
                'title' => $request->title,
                'body' => $request->body,
                'listed' => $request->listed
            ]
        );


        return ['success' => true, 'message' => 'Noticia creada con exito.'];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news, $id)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'title' => 'required|string',
                'body' => 'required|string',
                'listed' => 'required|boolean'
            ]
        );

        if ($validator->fails()) {
            return [
                'success' => false,
                'message' => $validator->errors()->first()
            ];
        }

        $news = News::findOrFail($id);
        $news->title = $request->title;
        $news->body = $request->body;
        $news->listed = $request->listed;

        $news->save();

        return ['success' => true, 'message' => 'Noticia actualizada con exito.'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news, $id, $uploadId)
    {
        News::findOrFail($id)->delete();
        $uploadObject = Upload::findOrFail($uploadId);
        Upload::destroyFile($uploadObject);
        return ['success' => true];
    }
}
