<?php

namespace App\Http\Controllers;

use App\Mail\ContactMail;
use App\Mail\LogContactMail;
use App\Models\ContactAddress;
use App\Models\ContactLog;
use Illuminate\Http\Request;
use Validator;
use Mail;
use Carbon\Carbon;

class ContactController extends Controller
{
    public function contact(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:3',
            'email' => 'required|email',
            'subject' => 'required|numeric',
            'phone' => 'nullable|string',
            'message' => 'required|string|min:1|max:500',
            'recaptcha' => 'required|string|min:100'
        ]);
        $validator->setAttributeNames([
            'name' => 'Nombre',
            'email' => 'Email',
            'subject' => 'Asunto',
            'phone' => 'Celular',
            'message' => 'Mensaje',
        ]);

        if ($validator->fails()) {
            return ['success' => false, 'message' => $validator->errors()->first()];
        }

        if (!$request->recaptcha) {
            return ['success' => false, 'message' => 'Ocurrió un error al obtener el recaptcha'];
        }

        $client = new \GuzzleHttp\Client();

        $response = $client->post('https://www.google.com/recaptcha/api/siteverify', [
            'form_params' => [
                'secret' => '6Ld3MH0UAAAAAEuAHTlgAUSAzAeEb7aQyuGrXPly',
                'response' => $request->recaptcha,
                'remoteip' => $request->ip()
            ]
        ]);

        $content = json_decode($response->getBody()->getContents(), true);

        if (!$content['success']) {
            return ['success' => false, 'message' => 'El captcha caducó o está duplicado'];
        }

        $contactAddress = ContactAddress::find($request->subject);

        if (!$contactAddress) {
            return ['success' => false, 'message' => 'El asunto no es válido'];
        }

        ContactLog::create([
            'name' => $request->name,
            'email' => $request->email,
            'contact_address_id' => $request->subject,
            'phone' => $request->phone,
            'message' => $request->message,
            'created_at' => Carbon::now(),
        ]);

        Mail::to($request->email)->send(new ContactMail($request->name));
        $sendTo = 'contacto@gicc.com.ar';
        Mail::to($sendTo)->send(new LogContactMail($request->all()));

        return ['success' => true, 'message' => 'Recibimos tu mensaje. Te contactaremos a la brevedad.'];
    }
}
