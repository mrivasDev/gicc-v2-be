<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\JWTAuth;

class JWT {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            \JWTAuth::parseToken()->authenticate();
        } catch(\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(null, 401);
        }
        return $next($request);
    }
}
