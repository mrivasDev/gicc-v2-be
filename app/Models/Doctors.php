<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Doctors extends Model
{
    protected $fillable = [
        'name', 'lastname', 'specialty', 'enrollment', 'image_id'
    ];

    protected $appends = ['url'];

    public function getUrlAttribute()
    {
    	if (!$this->image) {
            unset($this->image);
    		return null;
    	}
    	$file = $this->image->file;
        unset($this->image);
        return url('media/image/doctors/' . $file);
    }

    public function image()
    {
    	return $this->belongsTo(Upload::class, 'image_id', 'id');
    }

}
