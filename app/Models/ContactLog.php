<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactLog extends Model
{
	public $timestamps = false;
    protected $casts = [
		'created_at' => 'datetime'
	];
	protected $fillable = ['name', 'email', 'contact_address_id', 'phone', 'message', 'created_at'];
}
