<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactAddress extends Model
{
	protected $fillable = ['subject', 'email'];
    public $timestamps = false;

    public function contacts() {
    	return $this->hasMany(ContactLog::class);
    }
}
