<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{
	public $timestamps = false;
	
	protected $fillable = ['hash', 'type', 'name', 'folder', 'file', 'public', 'created_at'];
	
	protected $hidden = ['file'];
	
	protected $casts = [
		'public' => 'boolean'
	];
	

	private static function delete_directory($dirname) {
		if (is_dir($dirname))
		$dir_handle = opendir($dirname);
		if (!$dir_handle)
		return false;
		while($file = readdir($dir_handle)) {
			if ($file != "." && $file != "..") {
				if (!is_dir($dirname."/".$file))
				unlink($dirname."/".$file);
				else
				static::delete_directory($dirname.'/'.$file);
			}
		}
		closedir($dir_handle);
		rmdir($dirname);
		return true;
	}

	public static function boot()
	{
		parent::boot();
		static::creating(function ($model) {
			do {
				$model->hash = bin2hex(random_bytes(24));
			} while (Upload::where('hash', $model->hash)->exists());
			$model->created_at = $model->freshTimestamp();
		});
	}
	
	public static function destroyFile(Upload $uploadToDelete)
	{
		$upload = Upload::findOrFail($uploadToDelete->id);
		$path = $upload->public ? public_path() : storage_path() . '/' . 'app';
		$path .= '/' . $upload->type;
		$subFolder = explode('/',$upload->file)[0];
		$path .= $upload->folder ? ('/' . $upload->folder . '/' . $subFolder) : ('/' . $subFolder);
		
		static::delete_directory($path);

		return true;
	}
	
	
	
}
