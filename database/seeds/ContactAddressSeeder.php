<?php

use Illuminate\Database\Seeder;
use App\Models\ContactAddress;

class ContactAddressSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (ContactAddress::all()->count() > 0) {
            return;
        }

        $subjects = [
            'Administración',
            'Otro'
        ];
        foreach ($subjects as $subject) {
            ContactAddress::create([
                'subject' => $subject,
                'email' => 'contacto@gicc.com.ar'
            ]);
        }
    }
}
