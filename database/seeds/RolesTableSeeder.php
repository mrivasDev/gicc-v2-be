<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleSeeder extends Seeder
{
	/**
	* Run the database seeds.
	*
	* @return void
	*/
	public function run()
	{
		// Reset cached roles and permissions
		app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

		if (!$admin = Role::where('name', 'admin')->first()) {
			$admin = Role::create(['guard_name' => 'api', 'name' => 'admin']);
		}
		$admin->givePermissionTo(Permission::all());
	}
}
