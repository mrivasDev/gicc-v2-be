<?php

use Illuminate\Database\Seeder;
use App\Models\Doctors;

class DoctorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Doctors::create([
            'name' => "Carolina", 'lastname'=> "Llull", 'specialty' => "Bioquimica", 'enrollment' => 295
        ]);
        Doctors::create([
            'name' => "Paola", 'lastname'=> "Alba", 'specialty' => "Endocrinologia", 'enrollment' => 2096
        ]);
        Doctors::create([
            'name' => "Gabriela", 'lastname'=> "Rossi", 'specialty' => "Cardiologia Infantil", 'enrollment' => 1946
        ]);
        Doctors::create([
            'name' => "Eugenia", 'lastname'=> " OzinoCaligaris", 'specialty' => "Pediatria", 'enrollment' => 1699
        ]);
        Doctors::create([
            'name' => "Oscar", 'lastname'=> "Dahir", 'specialty' => "Cirugia Plastica", 'enrollment' => 2217
        ]);
        Doctors::create([
            'name' => "Fabricio", 'lastname'=> "Gravier", 'specialty' => "Cardiologia Adultos", 'enrollment' => 2014
        ]);
        Doctors::create([
            'name' => "Dario", 'lastname'=> "Guinder", 'specialty' => "OftalmologIa", 'enrollment' => 1742
        ]);
        Doctors::create([
            'name' => "Mariano", 'lastname'=> "Carluccio", 'specialty' => "Generalista", 'enrollment' => 1038
        ]);
        Doctors::create([
            'name' => "Javier", 'lastname'=> "Moradas", 'specialty' => "Medicina del Deporte y Nutricion", 'enrollment' => 1142
        ]);
        Doctors::create([
            'name' => "Sebastian ", 'lastname'=> "Mendelberg Rissi", 'specialty' => "Cirugia General", 'enrollment' => 1502
        ]);
        Doctors::create([
            'name' => "Martin", 'lastname'=> "Iturraspe", 'specialty' => "Clinica Medica", 'enrollment' => 2523
        ]);
        Doctors::create([
            'name' => "Victor ", 'lastname'=> "D' adam", 'specialty' => "Urologia", 'enrollment' => 1894
        ]);
        Doctors::create([
            'name' => "Walter", 'lastname'=> "Herrera", 'specialty' => "Ginecologia", 'enrollment' => 1337
        ]);
        Doctors::create([
            'name' => "Fabio", 'lastname'=> "Russo", 'specialty' => "Cardología Adultos", 'enrollment' => 1718
        ]);
        Doctors::create([
            'name' => "Diego", 'lastname'=> "Roca", 'specialty' => "Clinica Medica", 'enrollment' => 1562
        ]);
        Doctors::create([
            'name' => "Martin", 'lastname'=> "Morante", 'specialty' => "Clinica Medica y Cardiologia Adultos", 'enrollment' => 1752
        ]);
        Doctors::create([
            'name' => "Carlos", 'lastname'=> "Vallejo", 'specialty' => "Cardiologia Adultos e Infantil", 'enrollment' => 1025
        ]);
        Doctors::create([
            'name' => "Sandra", 'lastname'=> "Saavedra", 'specialty' => "Cardiologia Adultos", 'enrollment' => 1574
        ]);
        Doctors::create([
            'name' => "Marcelo", 'lastname'=> "Vassia", 'specialty' => "Gastroenterologia", 'enrollment' => 1490
        ]);
        Doctors::create([
            'name' => "Andrea", 'lastname'=> "Sosa", 'specialty' => "Ginecologia", 'enrollment' => 2502
        ]);
        Doctors::create([
            'name' => "Mariano", 'lastname'=> "Olviera", 'specialty' => "Odontologia", 'enrollment' => 409
        ]);
        Doctors::create([
            'name' => "Soledad", 'lastname'=> "Acevedo", 'specialty' => "Odontologia", 'enrollment' => 634
        ]);
        Doctors::create([
            'name' => "Paula", 'lastname'=> "Aberasturi", 'specialty' => "Otorrinolaringología", 'enrollment' => 1170
        ]);
        Doctors::create([
            'name' => "Florencia", 'lastname'=> "Lohidoy", 'specialty' => "Lic en Terapia Fisica-RPG-Stretching", 'enrollment' => 2934
        ]);
        Doctors::create([
            'name' => "Carlos", 'lastname'=> "Sbrocco", 'specialty' => "Cirugia Vascular Periferica", 'enrollment' => 1577
        ]);
        Doctors::create([
            'name' => "Jesica", 'lastname'=> "Lucero", 'specialty' => "Odontologia", 'enrollment' => 555
        ]);
        Doctors::create([
            'name' => "Eduardo", 'lastname'=> "Perrin", 'specialty' => "Cirugia bucomaxilofacial Implantes dentarios y patologia articular", 'enrollment' => 352
        ]);
        Doctors::create([
            'name' => "Marcos", 'lastname'=> "Diez", 'specialty' => "Urología", 'enrollment' => 1736
        ]);
        Doctors::create([
            'name' => "Rocio", 'lastname'=> "Gonzalez Vermeulen", 'specialty' => "Nutricionista", 'enrollment' => 2922
        ]);
    }
}
