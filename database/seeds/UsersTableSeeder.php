<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    User::create([
      'name' => 'Admin',
      'email' => 'admin@admin.com',
      'password' => Hash::make('admin'),
      'dni' => 00000000
    ])->assignRole('admin');

    // If production we could set other users
    //if (config('app.env') !== 'production') { }
  }
}
