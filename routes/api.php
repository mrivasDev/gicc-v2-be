<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::view('/', 'welcome');

Route::group(['prefix' => 'auth'], function () {
	Route::post('login', 'Auth\LoginController@login');
	Route::post('logout', 'Auth\LoginController@logout');
	Route::post('refresh', 'Auth\LoginController@refresh');
	Route::get('me', 'Auth\LoginController@me');
});

Route::middleware('auth:api')->group(function () {

	Route::prefix('upload')->group(function () {
		Route::get('{upload}', 'UploadController@show')->where('upload', '[0-9]+');
		Route::post('{type}/{public}/{folder?}', 'UploadController@upload');
		Route::get('generate/{hash}', 'UploadController@generate');
	});

	Route::prefix('users')->group(function () {
		Route::post('edit/{id}', 'UserController@update');
		Route::post('delete/{id}', 'UserController@destroy');
	});

	Route::apiResource('users', 'UserController');

	Route::prefix('doctors')->group(function () {
		Route::post('delete/{id}', 'DoctorsController@delete');
		Route::get('get/{id}', 'DoctorsController@show');
	});

	Route::apiResource('doctors', 'DoctorsController');


	Route::prefix('news')->group(function () {
		Route::post('delete/{id}', 'NewsController@delete');
		Route::get('get/{id}', 'NewsController@show');
	});

	Route::apiResource('news', 'NewsController');

});
