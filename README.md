# Project Title

ARTISAN - STARTER

### Installing

Install dependencies
 - composer install
 - sudo chgrp -R www-data storage bootstrap/cache
 - sudo chmod -R ug+rwx storage bootstrap/cache
 - composer du

Create key
 - php artisan key:generate

Configure .env file for DB access

Final touches
 - php artisan migrate
 - php artisan db:seed

Run the server
 - php artisan serve

## Useful commands

### To create migrations
 - php artisan make:migration create_new_entity_table
### To create seeder
 - php artisan make:seeder NewEntityTableSeeder
### To create model
 - php artisan make:model NewEntity -m
### To create controller and link it to the model
 - php artisan make:controller NewEntityController --api --model=NewEntity
### Don't know what to do?
 - php artisan list

## Deployment

No deployment so far

## Authors

* **Matias Rivas** - *Initial work* 

## License

No licence so far
